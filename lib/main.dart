import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:todolist/style.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: TodoPage(),
    );
  }
}

class TodoPage extends StatefulWidget {
  const TodoPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => TodoState();
}

class TodoState extends State<TodoPage> {
  int counter = 0;
  List<String> todoList = [
    "Belajar Flutter",
    "Mengerjakan tugas kelompok",
    "Beli makan siang bareng",
    "Bernafas tiap hari"
  ];
  TextEditingController todoCtrl = TextEditingController();

  void tampilForm(var context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text("Form Tambah Todo"),
              content: Column(
                children: [
                  TextField(
                    controller: todoCtrl,
                    decoration: const InputDecoration(hintText: "Todo baru"),
                  ),
                ],
              ),
              actions: [
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        todoList.add(todoCtrl.text);
                      });
                      todoCtrl.text = '';
                      Navigator.pop(context);
                    },
                    child: Text('Tambah'))
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Aplikasi Todo List')),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          tampilForm(context);
        },
        child: Icon(Icons.add_rounded),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: const Text('List Pekerjaan',
                  style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueAccent))),
          Expanded(
              child: ListView.builder(
                  itemCount: todoList.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: const Icon(Icons.check_circle_rounded),
                      title: Text(
                        todoList[index],
                        style: header4,
                      ),
                      trailing: IconButton(
                        icon: const Icon(Icons.delete, color: Colors.redAccent),
                        onPressed: () {
                          setState(() {
                            todoList.removeAt(index);
                          });
                        },
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}
